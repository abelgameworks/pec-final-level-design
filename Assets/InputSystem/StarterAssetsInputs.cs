using UnityEngine;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
using UnityEngine.InputSystem;
#endif

namespace StarterAssets
{
	public class StarterAssetsInputs : MonoBehaviour
	{
		[Header("Character Input Values")]
		public Vector2 move;
		public Vector2 look;
		public bool jump;
		public bool sprint;

		[Header("Movement Settings")]
		public bool analogMovement;

		[Header("Added Functionality")]
		public bool nextCamera;
		public bool previousCamera;
		public bool ThroughWalls;
		public bool Interaction;
		public bool DropItem;

#if !UNITY_IOS || !UNITY_ANDROID
		[Header("Mouse Cursor Settings")]
		public bool cursorLocked = true;
		public bool cursorInputForLook = true;
#endif

#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
		public void OnMove(InputValue value)
		{
			MoveInput(value.Get<Vector2>());
		}

		public void OnNextCamera(InputValue value)
		{
			NextCameraInput(value.isPressed);
		}

		public void OnPreviousCamera(InputValue value)
		{
			PreviousCameraInput(value.isPressed);
		}

		public void OnThroughWalls(InputValue value)
		{
			ThroughWallsInput(value.isPressed);
		}

		public void OnDropItem(InputValue value)
		{
			DropItemInput(value.isPressed);
		}

		public void OnInteraction(InputValue value)
		{
			InteractionInput(value.isPressed);
		}

		public void OnLook(InputValue value)
		{
			if(cursorInputForLook)
			{
				LookInput(value.Get<Vector2>());
			}
		}

		public void OnJump(InputValue value)
		{
			JumpInput(value.isPressed);
		}

		public void OnSprint(InputValue value)
		{
			SprintInput(value.isPressed);
		}
#else
	// old input sys if we do decide to have it (most likely wont)...
#endif
		public void NextCameraInput(bool value)
        {
			nextCamera = value;
        }

		public void PreviousCameraInput(bool value)
		{
			previousCamera = value;
		}

		public void ThroughWallsInput(bool value)
        {
			ThroughWalls = value;
        }

		public void DropItemInput(bool value)
		{
			DropItem = value;
		}

		public void InteractionInput(bool value)
		{
			Interaction = value;
		}

		public void MoveInput(Vector2 newMoveDirection)
		{
			move = newMoveDirection;
		} 

		public void LookInput(Vector2 newLookDirection)
		{
			look = newLookDirection;
		}

		public void JumpInput(bool newJumpState)
		{
			jump = newJumpState;
		}

		public void SprintInput(bool newSprintState)
		{
			sprint = newSprintState;
		}

#if !UNITY_IOS || !UNITY_ANDROID

		private void OnApplicationFocus(bool hasFocus)
		{
			SetCursorState(cursorLocked);
		}

		private void SetCursorState(bool newState)
		{
			Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
		}

#endif

	}
	
}