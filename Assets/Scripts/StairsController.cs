using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairsController : MonoBehaviour
{
    public GameObject[] Blocks;

    public bool moving = false;

    // Update is called once per frame
    void Update()
    {
        if (moving)
            Move();
    }

    public void Move()
    {
        moving = false;
        foreach (GameObject Block in Blocks)
        {
            MovableBlock movBlock = Block.GetComponent<MovableBlock>();
            movBlock.moving = true;
        }
    }
}
