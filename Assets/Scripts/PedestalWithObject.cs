using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PedestalWithObject : MonoBehaviour
{
    public float speed = 30f;
    private CanvasController canvas;
    private bool rotating = true;
    private ThirdPersonController controller;
    public MovableBlock[] puzzleElements;
    public Light MainLight;
    public GameObject[] candles;
    private GameplayManager gameplayManager;
    public GameObject sphere;
    public ObjectInteraction.PuzzleNumber type;

    public bool debug = false;

    private void Start()
    {
        canvas = GameObject.Find("Canvas").GetComponent<CanvasController>();
        controller = GameObject.Find("Player").GetComponent<ThirdPersonController>();
        gameplayManager = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (rotating)
            transform.Rotate(new Vector3(0, speed, 0) * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player") && rotating && gameplayManager.isCarrying && gameplayManager.itemType == type)
        {
            canvas.SetDrop(false);
            canvas.SetInteraction(true);

        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("Player") && controller.interactionPressed && rotating && gameplayManager.isCarrying && gameplayManager.itemType == type)
        {
            rotating = false;
            sphere.SetActive(true);
            gameplayManager.ReleaseObject(true, false);
            TurnOffPedestal();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
            canvas.SetInteraction(false);
        if (other.tag.Equals("Player") && !rotating && !gameplayManager.isCarrying && !sphere.activeInHierarchy)
            canvas.SetDrop(false);
    }

    private void TurnOffPedestal()
    {
        canvas.SetInteraction(false);
        foreach (MovableBlock block in puzzleElements)
        {
            block.moving = true;
        }

        if(type != ObjectInteraction.PuzzleNumber.Third)
            this.gameObject.GetComponent<MovableBlock>().moving = true;

        this.gameObject.GetComponent<Light>().enabled = false;
        MainLight.intensity -= 0.25f;
        StartCoroutine(TurnOnLights());
    }

    IEnumerator TurnOnLights()
    {
        canvas.SetDrop(false);
        for (int i = 0; i <= 50; i++)
        {
            foreach (GameObject candle in candles)
            {
                candle.GetComponent<Light>().intensity = i;
            }
            yield return new WaitForSeconds(0.2f);
        }
        canvas.SetDrop(false);
        Debug.Log("Finishing Puzzle");
        gameplayManager.finishPuzzle();
        StartCoroutine(CleanUp());
    }

    IEnumerator CleanUp()
    {
        yield return new WaitForSeconds(0.5f);
        foreach (MovableBlock block in puzzleElements)
        {
            Destroy(block.gameObject);
        }
    }
}
