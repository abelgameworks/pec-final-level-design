using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallEffect : MonoBehaviour
{
    public float speed = 30f;
    public float stopTime = 6f;
    private void Update()
    {
        transform.Rotate(new Vector3(0, speed, 0) * Time.deltaTime);
        Destroy(this.GetComponent<BallEffect>(), stopTime);
    }
}
