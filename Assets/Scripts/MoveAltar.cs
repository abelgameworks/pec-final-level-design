using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAltar : MonoBehaviour
{
    public Transform startMarker;
    public Transform endMarker;

    // Movement speed in units per second.
    public float speed = 0.001F;

    // Time when the movement started.
    private float startTime;

    // Total distance between the markers.
    private float journeyLength;

    public bool moving = false;

    public GameObject AltarLight;
    public GameObject NoPassCollider;

    void Start()
    {
        // Keep a note of the time the movement started.
        startTime = Time.time;

        // Calculate the journey length.
        journeyLength = Vector3.Distance(startMarker.position, endMarker.position);
    }

    // Move to the target end position.
    void Update()
    {
        if (moving)
        {
            // Distance moved equals elapsed time times speed..
            float distCovered = (Time.time - startTime) * speed;

            // Fraction of journey completed equals current distance divided by total distance.
            float fractionOfJourney = distCovered / journeyLength;

            // Set our position as a fraction of the distance between the markers.
            transform.position = Vector3.Lerp(startMarker.position, endMarker.position, fractionOfJourney);

            if(!AltarLight.activeInHierarchy)
                AltarLight.SetActive(true);

            if (transform.position == endMarker.position)
            {
                moving = false;
                Destroy(NoPassCollider);
            }
        }
    }
}
