using StarterAssets;
using System.Collections;
using UnityEngine;

public class SecondPuzzle : MonoBehaviour
{
    public StairsController StairsController;
    public GameObject WallToDestruct;
    private ThirdPersonController controller;
    private CanvasController canvas;
    public GameObject Pedestal;

    private void Start()
    {
        controller = GameObject.Find("Player").GetComponent<ThirdPersonController>();
        canvas = GameObject.Find("Canvas").GetComponent<CanvasController>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag.Equals("Player"))
            canvas.SetInteraction(true);
    }

    private void OnTriggerStay(Collider col)
    {
        if (col.tag.Equals("Player") && controller.interactionPressed)
        {
            canvas.SetInteraction(false);
            StairsController.Move();
            Destroy(WallToDestruct);
            Pedestal.GetComponent<MovableBlock>().moving = true;
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.tag.Equals("Player"))
            canvas.SetInteraction(false);
    }

    IEnumerator WaitForBlock()
    {
        yield return new WaitForSeconds(1f);
        Pedestal.GetComponent<BoxCollider>().enabled = true;
        Destroy(this);
    }
}
