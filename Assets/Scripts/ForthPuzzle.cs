using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForthPuzzle : MonoBehaviour
{
    public GameObject WallToDestruct;
    private ThirdPersonController controller;
    private CanvasController canvas;

    private void Start()
    {
        controller = GameObject.Find("Player").GetComponent<ThirdPersonController>();
        canvas = GameObject.Find("Canvas").GetComponent<CanvasController>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag.Equals("Player"))
            canvas.SetInteraction(true);
    }

    private void OnTriggerStay(Collider col)
    {
        if (col.tag.Equals("Player") && controller.interactionPressed)
        {
            Destroy(WallToDestruct);
            canvas.SetInteraction(false);
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.tag.Equals("Player"))
            canvas.SetInteraction(false);
    }

}
