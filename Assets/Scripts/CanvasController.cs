using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    public GameObject Interaction;
    public GameObject Drop;
    public GameObject PickUp;
    public GameObject Warning;

    public void SetInteraction(bool status)
    {
        Interaction.SetActive(status);
    }

    public void SetDrop(bool status)
    {
        Drop.SetActive(status);
    }

    public void SetPickUp(bool status)
    {
        PickUp.SetActive(status);
    }

    public void ShowWarning()
    {
        StartCoroutine(WarningRoutine());
    }

    IEnumerator WarningRoutine()
    {
        Warning.SetActive(true);
        yield return new WaitForSeconds(2.5f);
        Warning.SetActive(false);
    }
}
