using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInteraction : MonoBehaviour
{
    public enum PuzzleNumber { Third, Forth, None }
    public PuzzleNumber type;
    public CanvasController canvas;
    public GameplayManager gameplayManager;
    public ThirdPersonController controller;
    public bool grabbed = false;
    private Collider col;

    private void Start()
    {
        canvas = GameObject.Find("Canvas").GetComponent<CanvasController>();
        gameplayManager = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();
        controller = GameObject.Find("Player").GetComponent<ThirdPersonController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player") && !gameplayManager.isCarrying)
            canvas.SetPickUp(true);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("Player") && controller.interactionPressed && !gameplayManager.isCarrying)
        {
            gameplayManager.isCarrying = true;
            gameplayManager.itemType = type;
            gameplayManager.item = this.gameObject;
            gameplayManager.carryItem();
            canvas.SetPickUp(false);
            canvas.SetDrop(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            canvas.SetPickUp(false);
            if (col && col.isTrigger)
                col.isTrigger = false;
        }

    }

    public void ReenableBall(Collider coll)
    {
        col = coll;
    }
}
