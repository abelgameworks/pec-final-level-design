using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    public int puzzlesSolved = 0;
    public int puzzleNum = 4;
    public bool isCarrying = false;
    public Transform PlayerHand;
    public GameObject item;
    public ObjectInteraction.PuzzleNumber itemType = ObjectInteraction.PuzzleNumber.None;
    public GameObject PlotAltar;
    private CanvasController canvas;

    private void Start()
    {
        canvas = GameObject.Find("Canvas").GetComponent<CanvasController>();
    }

    public void carryItem()
    {
        item.transform.position = PlayerHand.position;
        item.transform.parent = PlayerHand;
    }

    public void ReleaseObject(bool destination, bool wall)
    {
        if (isCarrying && item)
        {
            if (destination)
            {
                item.transform.parent = null;
                Destroy(item);
                isCarrying = false;
            }
            else
            {
                isCarrying = false;
                item.transform.parent = null;
                SphereCollider coll = item.GetComponent<SphereCollider>();
                coll.isTrigger = enabled;
                item.gameObject.GetComponent<ObjectInteraction>().ReenableBall(coll);
                if(wall)
                    canvas.ShowWarning();
            }
            canvas.SetDrop(false);
        }
    }

    public void finishPuzzle()
    {
        Debug.Log(puzzlesSolved +"/"+ puzzleNum);
        puzzlesSolved++;
        if (puzzlesSolved == puzzleNum)
            EnablePlotAltar();
    }

    private void EnablePlotAltar()
    {
        PlotAltar.SetActive(true);
        PlotAltar.GetComponent<MoveAltar>().moving = true;
    }
}
