using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallPlayerDetector : MonoBehaviour
{
    private BoxCollider[] Colliders;
    private ThirdPersonController controller;

    private void Start()
    {
        Colliders = this.transform.parent.gameObject.GetComponents<BoxCollider>();
        controller = GameObject.Find("Player").GetComponent<ThirdPersonController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player") && controller.throughWalls)
        {
            foreach (BoxCollider collider in Colliders)
            {
                collider.isTrigger = true;
            }
        }
        controller.SetHelpLight(true);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("Player") && controller.throughWalls)
        {
            foreach (BoxCollider collider in Colliders)
            {
                collider.isTrigger = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            foreach (BoxCollider collider in Colliders)
            {
                collider.isTrigger = false;
            }
            controller.throughWalls = false;
        }
        controller.SetHelpLight(false);
    }
}
